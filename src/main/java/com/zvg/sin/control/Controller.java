package com.zvg.sin.control;

import com.zvg.sin.service.instance.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/")
public class Controller {
    private final ProductService productService;

    @Autowired
    public Controller(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("home")
    @Transactional
    public ModelAndView toIndex(Map<String, Object> model) {
        return new ModelAndView("index", model);
    }

    @GetMapping("account")
    public ModelAndView toAccount(Map<String, Object> model) {
        return new ModelAndView("account", model);
    }

    @GetMapping("checkout")
    public ModelAndView toCheckout(Map<String, Object> model) {
        return new ModelAndView("checkout", model);
    }

    @GetMapping("contact")
    public ModelAndView toContact(Map<String, Object> model) {
        return new ModelAndView("contact", model);
    }

    @GetMapping("products")
    public ModelAndView toProducts(Map<String, Object> model) {
        model.put("products", productService.getAll());
        return new ModelAndView("products", model);
    }

    @GetMapping("register")
    public ModelAndView toRegister(Map<String, Object> model) {
        return new ModelAndView("register", model);
    }

    @GetMapping("products/{id}/view")
    public ModelAndView toTypo(@PathVariable(name = "id") String id, Map<String, Object> model) {
        model.put("product", productService.getById(UUID.fromString(id)));
        return new ModelAndView("single", model);
    }
}
