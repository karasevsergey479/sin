package com.zvg.sin.service.instance;

import com.zvg.sin.service.model.IProductService;
import com.zvg.sin.sql.entity.Product;
import com.zvg.sin.sql.repo.IProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductService implements IProductService {
    private final IProductRepo productRepo;

    @Autowired
    public ProductService(IProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    public List<Product> getAll() {
        return productRepo.findAll();
    }

    public Product getById(UUID id) {
        return productRepo.getById(id);
    }
}
