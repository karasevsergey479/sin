package com.zvg.sin.sql.repo;

import com.zvg.sin.sql.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface IProductTypeRepo extends JpaRepository<Product, UUID> {
}
